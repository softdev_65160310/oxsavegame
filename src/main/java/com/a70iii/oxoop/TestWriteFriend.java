/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.a70iii.oxoop;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 *
 * @author Surap
 */
public class TestWriteFriend {
    public static void main(String[] args) throws FileNotFoundException, IOException{
        Friend f1 = new Friend("ANTOM", 20, "0888888888");
        Friend f2 = new Friend("BBR", 23, "0888888888");
      File file = new File("friends.dat");
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(f1);
        oos.writeObject(f2);
        oos.close();
        fos.close();
    }
}
